import components from './components'
import directives from './directives'

const install = (Vue, options = {}) => {

  directives.map(directive => {
    Vue.directive(directive.name, directive.action)
  })

  components.map(component => {
    Vue.component(component.name, component)
  })
}

export default install