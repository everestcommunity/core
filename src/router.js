import Vue from 'vue'
import Router from 'vue-router'
import Window from './components/Window.vue'
import Login from './views/Login.vue'
import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'login',
      title: 'Login',
      component: Login
    },
    {
      path: '/conteudos',
      name: 'conteudos',
      title: 'Conteúdos',
    },
    {
      path: '/conteudos',
      component: Window,
      children: [
        {
          path: 'paginas',
          name: 'conteudos/paginas',
          title: 'Páginas',
        },
      ]
    },
    {
      path: '/conteudos',
      component: Window,
      children: [
        {
          path: 'paginas/:name',
          name: 'conteudos/paginas/pagina',
          component: Home,
        },
      ]
    }
  ]
})
